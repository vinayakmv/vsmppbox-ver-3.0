/* ==================================================================== 
 * VSMPPBOX Software License, Version 1.0 
 * 
 * Copyright (c) 2016 Evoxtel Telecommunication 
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are not permitted 
 * 
 * This product includes software developed by the Kannel Group (http://www.kannel.org/).
 * 
 */ 

#ifndef SMPP_DATABASE_H
#define SMPP_DATABASE_H

#define SMPP_DATABASE_BATCH_LIMIT 1000

#ifdef __cplusplus
extern "C" {
#endif
    typedef struct {
        Msg *msg;
        unsigned long global_id;
        SMPPServer *smpp_server;
        long wakeup_thread_id;
    } SMPPDatabaseMsg;
    
    
    
    typedef struct {
        SMPPESMEAuthResult *(*authenticate) (void *context, Octstr *system_id, Octstr *password);
        int (*add_message)(SMPPServer *context, Msg *msg);      
        int (*log_message)(SMPPServer *context, Msg *msg, double cost); 
        int (*add_pdu)(SMPPServer *context, SMPPQueuedPDU *smpp_queued_pdu);      
        List *(*get_stored)(SMPPServer *context, long sms_type, Octstr *service, long limit);
        List *(*get_stored_pdu)(SMPPServer *context, Octstr *service, long limit);
        List *(*get_routes)(SMPPServer *context, int direction, Octstr *service);
        List *(*get_dest_prefix)(SMPPServer *context, Octstr *service);
        List *(*get_ndnc_smsc)(SMPPServer *context, Octstr *service);
        List *(*get_spam)(SMPPServer *context, Octstr *service);
        List *(*get_template)(SMPPServer *context, Octstr *service);
        List *(*get_sender)(SMPPServer *context, Octstr *service);
        int (*deduct_credit)(SMPPServer *context, Octstr *service, double value);
        int (*check_ndnc)(SMPPServer *context, Octstr *receiver);
        int (*check_priority_lead)(SMPPServer *context, Octstr *receiver, Octstr *service);
        int (*delete)(SMPPServer *context, unsigned long global_id, int temporary);
        List *(*get_esmes_with_queued)(SMPPServer *smpp_server);
        void (*shutdown)(SMPPServer *context);
        void *context;
        Dict *pending_pdu;
        Dict *pending_msg;
        
    } SMPPDatabase;
    
    
    SMPPDatabaseMsg *smpp_database_msg_create();
    void smpp_database_msg_destroy();

    SMPPDatabase *smpp_database_create();
    void smpp_database_destroy(SMPPDatabase *smpp_database);
            

    void *smpp_database_init(SMPPServer *smpp_server);
    void smpp_database_shutdown(SMPPServer *smpp_server);
    
    void *smpp_database_mysql_init(SMPPServer *smpp_server);
    
    SMPPESMEAuthResult *smpp_database_auth(SMPPServer *smpp_server, Octstr *username, Octstr *password);
    
    int smpp_database_add_message(SMPPServer *smpp_server, Msg *msg);
    int smpp_database_log_message(SMPPServer *smpp_server, Msg *msg, double cost);
    int smpp_database_add_pdu(SMPPServer *smpp_server, SMPPQueuedPDU *smpp_queued_pdu);
    List *smpp_database_get_stored(SMPPServer *smpp_server, long sms_type, Octstr *service, long limit);
    List *smpp_database_get_stored_pdu(SMPPServer *smpp_server, Octstr *service, long limit);
    List *smpp_database_get_routes(SMPPServer *smpp_server, int direction, Octstr *service);
    List *smpp_database_get_dest_prefix(SMPPServer *smpp_server, Octstr *service);
    List *smpp_database_get_ndnc_smsc(SMPPServer *smpp_server, Octstr *service);
    List *smpp_database_get_spam(SMPPServer *smpp_server, Octstr *service);
    List *smpp_database_get_template(SMPPServer *smpp_server, Octstr *service);
    List *smpp_database_get_sender(SMPPServer *smpp_server, Octstr *service);
    int smpp_database_deduct_credit(SMPPServer *smpp_server, Octstr *service, double value);
    int smpp_database_check_ndnc(SMPPServer *smpp_server, Octstr *receiver);
    int smpp_database_check_priority_lead(SMPPServer *smpp_server, Octstr *receiver, Octstr *service);
    List *smpp_database_get_esmes_with_queued(SMPPServer *smpp_server);
    
    int smpp_database_remove(SMPPServer *smpp_server, unsigned long global_id, int temporary);


#ifdef __cplusplus
}
#endif

#endif /* SMPP_DATABASE_H */

