/* ==================================================================== 
 * VSMPPBOX Software License, Version 1.0 
 * 
 * Copyright (c) 2016 Evoxtel Telecommunication 
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are not permitted 
 * 
 * This product includes software developed by the Kannel Group (http://www.kannel.org/).
 * 
 */ 

#include "gwlib/gwlib.h"


Octstr *smpp_uuid_create() {
    char buffer[UUID_STR_LEN + 1];
    uuid_t my_uuid;

    uuid_generate(my_uuid);
    uuid_unparse(my_uuid, buffer);

    return octstr_create(buffer);

}


Octstr *smpp_uuid_get(uuid_t my_uuid) {
    char buffer[UUID_STR_LEN + 1];

    uuid_unparse(my_uuid, buffer);

    return octstr_create(buffer);

}
