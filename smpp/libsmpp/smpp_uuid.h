/* ==================================================================== 
 * VSMPPBOX Software License, Version 1.0 
 * 
 * Copyright (c) 2016 Evoxtel Telecommunication 
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are not permitted 
 * 
 * This product includes software developed by the Kannel Group (http://www.kannel.org/).
 * 
 */ 

#ifndef SMPP_UUID_H
#define SMPP_UUID_H

#ifdef __cplusplus
extern "C" {
#endif

    Octstr *smpp_uuid_create();
    Octstr *smpp_uuid_get(uuid_t my_uuid);


#ifdef __cplusplus
}
#endif

#endif /* SMPP_UUID_H */

