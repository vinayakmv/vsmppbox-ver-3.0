/* ==================================================================== 
 * VSMPPBOX Software License, Version 1.0 
 * 
 * Copyright (c) 2016 Evoxtel Telecommunication 
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are not permitted 
 * 
 * This product includes software developed by the Kannel Group (http://www.kannel.org/).
 * 
 */
 
#ifndef SMPP_PLUGIN_H
#define SMPP_PLUGIN_H

#ifdef __cplusplus
extern "C" {
#endif
    typedef struct SMPPPlugin SMPPPlugin;

    struct SMPPPlugin {
        Octstr *id;
        Octstr *args;
        SMPPESMEAuthResult *(*authenticate)(SMPPPlugin *smpp_plugin, Octstr *system_id, Octstr *password);
        void (*route_message)(SMPPPlugin *smpp_plugin, int direction, Octstr *smsc_id, Octstr *system_id, Msg *msg, void(*callback)(void *context, SMPPRouteStatus *smpp_route_status), void *context);
        int (*init)(SMPPPlugin *smpp_plugin);
        void (*reload)(SMPPPlugin *smpp_plugin);
        void (*shutdown)(SMPPPlugin *smpp_plugin);
        void *context;
    };
    
    SMPPPlugin *smpp_plugin_init(SMPPServer *smpp_server, Octstr *id);
    void smpp_plugin_destroy(SMPPPlugin *smpp_plugin);
    void smpp_plugin_destroy_real(SMPPPlugin *smpp_plugin);
    
    



#ifdef __cplusplus
}
#endif

#endif /* SMPP_PLUGIN_H */