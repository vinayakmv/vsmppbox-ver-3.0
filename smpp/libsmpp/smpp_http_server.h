/* ==================================================================== 
 * VSMPPBOX Software License, Version 1.0 
 * 
 * Copyright (c) 2016 Evoxtel Telecommunication 
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are not permitted 
 * 
 * This product includes software developed by the Kannel Group (http://www.kannel.org/).
 * 
 */

#ifndef SMPP_HTTP_SERVER_H
#define SMPP_HTTP_SERVER_H

#ifdef __cplusplus
extern "C" {
#endif

#define HTTP_CONTENT_TYPE_PLAIN 0
#define HTTP_CONTENT_TYPE_XML 1
    
    typedef struct {
        Octstr *result;
        List *headers;
        int status;
    } SMPPHTTPCommandResult;

    typedef struct {
        Octstr *key;
        SMPPHTTPCommandResult *(*callback)(SMPPServer *smpp_server, List *cgivars, int content_type);
    } SMPPHTTPCommand;


    void smpp_http_server_init(SMPPServer *smpp_server);
    void smpp_http_server_shutdown(SMPPServer *smpp_server);
    
    SMPPHTTPCommandResult *smpp_http_command_result_create();
    SMPPHTTPCommand *smpp_http_server_command_create();
    void smpp_http_server_add_command(SMPPServer *smpp_server, Octstr *key, SMPPHTTPCommandResult *(*callback)(SMPPServer *smpp_server, List *cgivars, int content_type));

#ifdef __cplusplus
}
#endif

#endif /* SMPP_HTTP_SERVER_H */

