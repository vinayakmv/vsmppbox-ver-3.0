/* ==================================================================== 
 * VSMPPBOX Software License, Version 1.0 
 * 
 * Copyright (c) 2016 Evoxtel Telecommunication 
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are not permitted 
 * 
 * This product includes software developed by the Kannel Group (http://www.kannel.org/).
 * 
 */ 

#ifndef SMPP_QUEUE_H
#define SMPP_QUEUE_H

#ifdef __cplusplus
extern "C" {
#endif
    void smpp_queues_callback_deliver_sm_resp(void *context, long status);
    
    int smpp_queues_add_outbound(SMPPQueuedPDU *smpp_queued_pdu);
    int smpp_queues_add_inbound(SMPPQueuedPDU *smpp_queued_pdu);
    
    void smpp_queues_send_enquire_link(SMPPEsme *smpp_esme);
    
    int smpp_queues_pdu_compare(const void *a, const void *b);
    void smpp_queues_init(SMPPServer *smpp_server);
    void smpp_queues_shutdown(SMPPServer *smpp_server);




#ifdef __cplusplus
}
#endif

#endif /* SMPP_QUEUE_H */

