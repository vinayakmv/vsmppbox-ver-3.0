/* ==================================================================== 
 * VSMPPBOX Software License, Version 1.0 
 * 
 * Copyright (c) 2016 Evoxtel Telecommunication 
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are not permitted 
 * 
 * This product includes software developed by the Kannel Group (http://www.kannel.org/).
 * 
 */ 

#ifndef SMPP_LISTENER_H
#define SMPP_LISTENER_H

#ifdef __cplusplus
extern "C" {
#endif
    int smpp_listener_start(SMPPServer *smpp_server);
    void smpp_listener_shutdown(SMPPServer *smpp_server);

    void smpp_listener_auth_failed(SMPPServer *smpp_server, Octstr *ip);
    int smpp_listener_ip_is_blocked(SMPPServer *smpp_server, Octstr *ip);


#ifdef __cplusplus
}
#endif

#endif /* SMPP_LISTENER_H */

