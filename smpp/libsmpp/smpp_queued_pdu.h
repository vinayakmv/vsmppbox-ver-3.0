/* ==================================================================== 
 * VSMPPBOX Software License, Version 1.0 
 * 
 * Copyright (c) 2016 Evoxtel Telecommunication 
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are not permitted 
 * 
 * This product includes software developed by the Kannel Group (http://www.kannel.org/).
 * 
 */ 

#ifndef SMPP_QUEUED_PDU_H
#define SMPP_QUEUED_PDU_H

#ifdef __cplusplus
extern "C" {
#endif
    
#define SMPP_QUEUED_PDU_DESTROYED 0x04FE

    typedef struct {
        SMPP_PDU *pdu;
        SMPPEsme *smpp_esme;
        long priority;
        int disconnect; /* If this flag is set the connection will be free'd/disconnected after its sent */
        Octstr *id;
        Octstr *system_id; /* If the esme is null, this is used to route deliver_sm/data_sm return PDU's */
        void (*callback)(void *context, long status);
        void *context;
        long time_sent;
        SMPPBearerbox *bearerbox;
        Octstr *bearerbox_id;
        SMPPServer *smpp_server;
        long sequence;
        unsigned long global_id;
        Msg *msg;
    } SMPPQueuedPDU;
    
    SMPPQueuedPDU *smpp_queued_pdu_create();
    SMPPQueuedPDU *smpp_queued_pdu_create_quick(SMPPEsme *smpp_esme, unsigned long type, unsigned long seq_no);
    void smpp_queued_pdu_destroy(SMPPQueuedPDU *smpp_queued_pdu);
    List *smpp_pdu_msg_to_pdu(SMPPEsme *smpp_esme, Msg *msg);



#ifdef __cplusplus
}
#endif

#endif /* SMPP_QUEUED_PDU_H */

