/* ==================================================================== 
 * VSMPPBOX Software License, Version 1.0 
 * 
 * Copyright (c) 2016 Evoxtel Telecommunication 
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are not permitted 
 * 
 * This product includes software developed by the Kannel Group (http://www.kannel.org/).
 * 
 */  

#ifndef SMPP_PDU_UTIL_H
#define SMPP_PDU_UTIL_H

#ifdef __cplusplus
extern "C" {
#endif

    Msg *smpp_submit_sm_to_msg(SMPPEsme *smpp_esme, SMPP_PDU *pdu, long *reason);
    Msg *smpp_data_sm_to_msg(SMPPEsme *smpp_esme, SMPP_PDU *pdu, long *reason);
    Octstr *smpp_pdu_get_system_id_from_dlr_url(Octstr *received_dlr_url);
    List *smpp_pdu_msg_to_pdu(SMPPEsme *smpp_esme, Msg *msg);


#ifdef __cplusplus
}
#endif

#endif /* SMPP_PDU_UTIL_H */

