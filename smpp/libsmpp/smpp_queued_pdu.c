/* ==================================================================== 
 * VSMPPBOX Software License, Version 1.0 
 * 
 * Copyright (c) 2016 Evoxtel Telecommunication 
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are not permitted 
 * 
 * This product includes software developed by the Kannel Group (http://www.kannel.org/).
 * 
 */ 

#include "gwlib/gwlib.h"
#include "gw/smsc/smpp_pdu.h"
#include "gw/msg.h"
#include "gw/load.h"
#include "smpp_server.h"
#include "smpp_esme.h"
#include "smpp_bearerbox.h"
#include "smpp_queued_pdu.h"




SMPPQueuedPDU *smpp_queued_pdu_create() {
    SMPPQueuedPDU *smpp_queued_pdu = gw_malloc(sizeof(SMPPQueuedPDU));
    smpp_queued_pdu->pdu = NULL;
    smpp_queued_pdu->smpp_esme = NULL;
    smpp_queued_pdu->priority = 0;
    smpp_queued_pdu->disconnect = 0;
    smpp_queued_pdu->id = NULL;
    smpp_queued_pdu->system_id = NULL;
    smpp_queued_pdu->callback = NULL;
    smpp_queued_pdu->context = NULL;
    smpp_queued_pdu->bearerbox = NULL;
    smpp_queued_pdu->bearerbox_id = NULL;
    smpp_queued_pdu->smpp_server = NULL;
    smpp_queued_pdu->time_sent = 0;
    smpp_queued_pdu->sequence = 0;
    smpp_queued_pdu->global_id = 0;
    return smpp_queued_pdu;
}

SMPPQueuedPDU *smpp_queued_pdu_create_quick(SMPPEsme *smpp_esme, unsigned long type, unsigned long seq_no) {
    SMPPQueuedPDU *smpp_queued_pdu = smpp_queued_pdu_create();
    smpp_queued_pdu->smpp_esme = smpp_esme;
    smpp_queued_pdu->pdu = smpp_pdu_create(type, seq_no);
    smpp_queued_pdu->id = NULL;
    smpp_queued_pdu->system_id = octstr_duplicate(smpp_esme->system_id);
    smpp_queued_pdu->smpp_server = smpp_esme->smpp_server;
    return smpp_queued_pdu;
}

void smpp_queued_pdu_destroy(SMPPQueuedPDU *smpp_queued_pdu) {
    smpp_pdu_destroy(smpp_queued_pdu->pdu);
    octstr_destroy(smpp_queued_pdu->id);
    octstr_destroy(smpp_queued_pdu->system_id);
    octstr_destroy(smpp_queued_pdu->bearerbox_id);
    gw_free(smpp_queued_pdu);
}
