/* ==================================================================== 
 * VSMPPBOX Software License, Version 1.0 
 * 
 * Copyright (c) 2016 Evoxtel Telecommunication 
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are not permitted 
 * 
 * This product includes software developed by the Kannel Group (http://www.kannel.org/).
 * 
 */  

#ifndef SMPP_HTTP_CLIENT_H
#define SMPP_HTTP_CLIENT_H

#ifdef __cplusplus
extern "C" {
#endif

#define SMPP_HTTP_HEADER_PREFIX "X-KSMPPD-"
#define HTTP_DEFAULT_MAX_OUTSTANDING 512

    SMPPESMEAuthResult *smpp_http_client_auth(SMPPServer *smpp_server, Octstr *system_id, Octstr *password);
    void smpp_http_client_route_init(SMPPServer *smpp_server);

#ifdef __cplusplus
}
#endif

#endif /* SMPP_HTTP_CLIENT_H */

