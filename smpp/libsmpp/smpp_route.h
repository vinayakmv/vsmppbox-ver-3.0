/* ==================================================================== 
 * VSMPPBOX Software License, Version 1.0 
 * 
 * Copyright (c) 2016 Evoxtel Telecommunication 
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are not permitted 
 * 
 * This product includes software developed by the Kannel Group (http://www.kannel.org/).
 * 
 */ 

#ifndef SMPP_ROUTE_H
#define	SMPP_ROUTE_H

#include "gwlib/regex.h"

#ifdef	__cplusplus
extern "C" {
#endif
    
    
#define SMPP_ROUTE_DIRECTION_UNKNOWN 0
#define SMPP_ROUTE_DIRECTION_OUTBOUND 1
#define SMPP_ROUTE_DIRECTION_INBOUND 2

#define SMPP_ROUTING_METHOD_DATABASE 1    
#define SMPP_ROUTING_METHOD_HTTP 2
#define SMPP_ROUTING_METHOD_PLUGIN 4
#define SMPP_ROUTING_DEFAULT_METHOD SMPP_ROUTING_METHOD_DATABASE
    
    typedef struct {
        long parts;
        double cost;
        int status;
    } SMPPRouteStatus;

    typedef struct {
        regex_t *regex;
        Octstr *system_id;
        Octstr *smsc_id;
        double cost;
        int direction;
        void *context;
        regex_t *source_regex;
    } SMPPRoute;

    typedef struct {
        regex_t *regex;
        Octstr *system_id;
        void *context;
    } SMPPSpam;

    typedef struct {
        regex_t *regex;
        Octstr *system_id;
        void *context;
    } SMPPTemplate;

    typedef struct {
        regex_t *regex;
        Octstr *system_id;
        void *context;
    } SMPPSender;
    
    typedef struct {
        Octstr *system_id;
        List *routes;
        RWLock *lock;
    } SMPPOutboundRoutes;
    
    typedef struct {
        Octstr *system_id;
        Octstr *dest_prefix;
    } SMPPDestPrefix;
    
    typedef struct {
        Octstr *system_id;
        Octstr *ndnc_smsc;
    } SMPPNdncSmsc;
    
    
    typedef struct {
        Dict *outbound_routes;
        List *inbound_routes;
        void (*route_message)(SMPPServer *smpp_server, int direction, Octstr *smsc_id, Octstr *system_id, Msg *msg, void(*callback)(void *context, SMPPRouteStatus *smpp_route_status), void *context);
        void (*reload)(SMPPServer *smpp_server);
        void (*shutdown)(SMPPServer *smpp_server);
        void (*init)(SMPPServer *smpp_server);
        RWLock *lock;
        RWLock *outbound_lock;
        int initialized;
        Octstr *http_routing_url;
        void *context;
    } SMPPRouting;

    
    void smpp_route_message_database(SMPPServer *smpp_server, int direction, Octstr *smsc_id, Octstr *system_id, Msg *msg, void(*callback)(void *context, SMPPRouteStatus *smpp_route_status), void *context);
    int smpp_add_dest_prefix_database(SMPPServer *smpp_server, Octstr *system_id, Msg *msg);   
    int smpp_add_ndnc_smsc_database(SMPPServer *smpp_server, Octstr *system_id, Msg *msg);
    int smpp_spam_message_database(SMPPServer *smpp_server, Octstr *system_id, Msg *msg, void *context);
    int smpp_template_message_database(SMPPServer *smpp_server, Octstr *system_id, Msg *msg, void *context);
    int smpp_sender_message_database(SMPPServer *smpp_server, Octstr *system_id, Msg *msg, void *context);
    

    void smpp_route_init(SMPPServer *smpp_server);
    void smpp_route_shutdown(SMPPServer *smpp_server);
    void smpp_route_rebuild(SMPPServer *smpp_server);
    void smpp_route_message(SMPPServer *smpp_server, int direction, Octstr *smsc_id, Octstr *system_id, Msg *msg, void(*callback)(void *context, SMPPRouteStatus *smpp_route_status), void *context);
    
    SMPPRoute *smpp_route_create();
    void smpp_route_destroy(SMPPRoute *smpp_route);
    
    SMPPDestPrefix *smpp_dest_create();
    void smpp_dest_destroy(SMPPDestPrefix *smpp_dest);
    
    SMPPNdncSmsc *smpp_ndnc_smsc_create();
    void smpp_ndnc_smsc_destroy(SMPPNdncSmsc *smpp_ndnc_smsc);

    SMPPSpam *smpp_spam_create();
    void smpp_spam_destroy(SMPPSpam *smpp_spam);

    SMPPTemplate *smpp_template_create();
    void smpp_template_destroy(SMPPTemplate *smpp_template);

    SMPPSender *smpp_sender_create();
    void smpp_sender_destroy(SMPPSender *smpp_sender);
    
    SMPPRouteStatus *smpp_route_status_create(Msg *msg);
    void smpp_route_status_destroy(SMPPRouteStatus *smpp_route_status);


#ifdef	__cplusplus
}
#endif

#endif	/* SMPP_ROUTE_H */

