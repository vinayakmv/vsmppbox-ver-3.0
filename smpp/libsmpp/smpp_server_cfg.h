/* ==================================================================== 
 * VSMPPBOX Software License, Version 1.0 
 * 
 * Copyright (c) 2016 Evoxtel Telecommunication 
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are not permitted 
 * 
 * This product includes software developed by the Kannel Group (http://www.kannel.org/).
 * 
 */ 

#ifndef SMPP_SERVER_CFG_H
#define SMPP_SERVER_CFG_H

#ifdef __cplusplus
extern "C" {
#endif

    int smpp_server_cfg_is_allowed_in_group(Octstr *group, Octstr *variable);
    int smpp_server_cfg_is_single_group(Octstr *query);


#ifdef __cplusplus
}
#endif

#endif /* SMPP_SERVER_CFG_H */

