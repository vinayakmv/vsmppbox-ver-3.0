/* ==================================================================== 
 * VSMPPBOX Software License, Version 1.0 
 * 
 * Copyright (c) 2016 Evoxtel Telecommunication 
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are not permitted 
 * 
 * This product includes software developed by the Kannel Group (http://www.kannel.org/).
 * 
 */ 
#ifndef SMPP_SERVER_H
#include <signal.h>

#define SMPP_SERVER_H
#define SMPP_SERVER_NAME "VSMPPBOX"
#define SMPP_SERVER_VERSION "2.0"

#define SMPP_SERVER_AUTH_METHOD_DATABASE 1
#define SMPP_SERVER_AUTH_METHOD_HTTP 2
#define SMPP_SERVER_AUTH_METHOD_PLUGIN 4

#define SMPP_SERVER_STATUS_STARTUP 1
#define SMPP_SERVER_STATUS_RUNNING 2
#define SMPP_SERVER_STATUS_LOG_REOPEN 4
#define SMPP_SERVER_STATUS_SHUTDOWN 8

#ifdef __cplusplus
extern "C" {
#endif

    typedef struct {
        Cfg *running_configuration;
        Octstr *config_filename;
        RWLock *config_lock;
        Octstr *server_id;
        
        List *bearerbox_outbound_queue;
        List *bearerbox_inbound_queue;
        
        int configured;
        
        volatile sig_atomic_t server_status;
        
        long smpp_port;
        
        int enable_ssl;
        
        void *esme_data;
        
        gw_prioqueue_t *inbound_queue;
        gw_prioqueue_t *outbound_queue;
        gw_prioqueue_t *simulation_queue;
        
        long num_inbound_queue_threads;
        long num_outbound_queue_threads;
        
        Counter *running_threads;
        
        Octstr *database_type;
        Octstr *database_config;
        Octstr *database_user_table;
        Octstr *database_ndnc_table;
		  Octstr *database_priority_lead_table;
        Octstr *database_store_table;
        Octstr *database_log_table;
        Octstr *database_mo_table;
        Octstr *database_mt_table;
        Octstr *database_dlr_table;
        Octstr *database_pdu_table;
        Octstr *database_route_table;
        Octstr *database_spam_table;
        Octstr *database_template_table;
        Octstr *database_sender_table;
        Octstr *database_version_table;
        
        int database_enable_queue;
        
        
        void *database;
        void *bearerbox;
        void *routing;
        void *http_server;
        void *http_client;
        
        
        
        struct event_base *event_base;
        struct evconnlistener *event_listener;
        
        Counter *esme_counter;
        long authentication_method;
        Octstr *auth_url;
        
        struct SMPPPlugin *plugin_auth;
        struct SMPPPlugin *plugin_route;
        
        
        Dict *plugins;
        Dict *ip_blocklist;
        RWLock *ip_blocklist_lock;
        long ip_blocklist_time;
        long ip_blocklist_attempts;
        
        long default_max_open_acks;
    } SMPPServer;
    
    SMPPServer *smpp_server_create();
    void smpp_server_destroy(SMPPServer *smpp_server);
    int smpp_server_reconfigure(SMPPServer *smpp_server);

#ifdef __cplusplus
}
#endif

#endif /* SMPP_SERVER_H */

