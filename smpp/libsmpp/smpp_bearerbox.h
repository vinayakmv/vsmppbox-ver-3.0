/* ==================================================================== 
 * VSMPPBOX Software License, Version 1.0 
 * 
 * Copyright (c) 2016 Evoxtel Telecommunication 
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are not permitted 
 * 
 * This product includes software developed by the Kannel Group (http://www.kannel.org/).
 * 
 */ 

#ifndef SMPP_BEARERBOX_H
#define SMPP_BEARERBOX_H

#define HEARTBEAT_INTERVAL 10

#ifdef __cplusplus
extern "C" {
#endif
    typedef struct {
        Msg *msg;
        void (*callback)(void *, int);
        void *context;
        Octstr *id;
    } SMPPBearerboxMsg;
    
    typedef struct {
        List *bearerboxes;
        gw_prioqueue_t *outbound_queue;
        gw_prioqueue_t *inbound_queue;
        RWLock *lock;
        SMPPServer *smpp_server;
        Dict *pending_requeues;
        long requeue_thread_id;
    } SMPPBearerboxState;

    typedef struct {
        Octstr *id;
        Octstr *host;
        long port;
        int ssl;
        Connection *connection;
        long last_msg;
        volatile sig_atomic_t alive;
        volatile sig_atomic_t writer_alive;
        SMPPBearerboxState *smpp_bearerbox_state;
        RWLock *lock;
        Dict *open_acks;        
        RWLock *ack_lock;
    } SMPPBearerbox;
    
    void smpp_bearerbox_init(SMPPServer *smpp_server);
    void smpp_bearerbox_shutdown(SMPPServer *smpp_server);
    
    void smpp_bearerbox_add_message(SMPPServer *smpp_server, Msg *msg, void(*callback)(void*,int), void *context);
    SMPPBearerboxMsg *smpp_bearerbox_msg_create(Msg *msg, void(*callback)(void*,int), void *context);
    void smpp_bearerbox_msg_destroy(SMPPBearerboxMsg *smpp_bearerbox_msg);
    int smpp_bearerbox_acknowledge(SMPPBearerbox *smpp_bearerbox, Octstr *id, ack_status_t status);



#ifdef __cplusplus
}
#endif

#endif /* SMPP_BEARERBOX_H */

