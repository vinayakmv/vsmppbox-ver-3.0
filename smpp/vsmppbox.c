/* ==================================================================== 
 * VSMPPBOX Software License, Version 1.0 
 * 
 * Copyright (c) 2016 Evoxtel Technologies 
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are not permitted 
 * 
 * This product includes software developed by the Kannel Group (http://www.kannel.org/).
 * 
 */ 
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#include "gwlib/gwlib.h"
#include "gw/heartbeat.h"
#include "libsmpp/smpp_server.h"
#include "libsmpp/smpp_listener.h"

/* Global context for the SMPP Server */
static SMPPServer *smpp_server_global;


static void signal_handler(int signum) {
    /* On some implementations (i.e. linuxthreads), signals are delivered
     * to all threads.  We only want to handle each signal once for the
     * entire box, and we let the gwthread wrapper take care of choosing
     * one.
     */
    if (!gwthread_shouldhandlesignal(signum))
        return;

    switch (signum) {
        case SIGINT:
        case SIGTERM:
       	    if (!(smpp_server_global->server_status & SMPP_SERVER_STATUS_SHUTDOWN)) {
                error(0, "SIGINT received, aborting program...");
                smpp_server_global->server_status |= SMPP_SERVER_STATUS_SHUTDOWN;
                smpp_listener_shutdown(smpp_server_global);
                gwthread_wakeup_all();
            }
            break;

        case SIGUSR2:
            warning(0, "SIGUSR2 received, catching and re-opening logs");
            log_reopen();
            alog_reopen();
            break;
        case SIGHUP:
            warning(0, "SIGHUP received, catching and re-opening logs");
            log_reopen();
            alog_reopen();
            break;

        /* 
         * It would be more proper to use SIGUSR1 for this, but on some
         * platforms that's reserved by the pthread support. 
         */
        case SIGQUIT:
	       warning(0, "SIGQUIT ±received, reporting memory usage.");
	       gw_check_leaks();
	       break;
               
        case SIGSEGV:
            panic(0, "SIGSEGV received, exiting immediately");
            break;
    }
}


static void setup_signal_handlers(void) {
    struct sigaction act;

    act.sa_handler = signal_handler;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    sigaction(SIGINT, &act, NULL);
    sigaction(SIGTERM, &act, NULL);
    sigaction(SIGQUIT, &act, NULL);
    sigaction(SIGHUP, &act, NULL);
    sigaction(SIGPIPE, &act, NULL);
    sigaction(SIGUSR2, &act, NULL);
//    sigaction(SIGSEGV, &act, NULL);
}

static int check_args(int i, int argc, char **argv) {
    return 0;
} 


/*
 * 
 */int main(int argc, char **argv)
{
    int cf_index;

    gwlib_init();
    cf_index = get_and_set_debugs(argc, argv, check_args);
    setup_signal_handlers();


    SMPPServer *smpp_server = smpp_server_create();
    
    smpp_server_global = smpp_server;
    
    smpp_server->server_status = SMPP_SERVER_STATUS_STARTUP;
    
    
    if (argv[cf_index] == NULL)
	smpp_server->config_filename = octstr_create("vsmppbox.conf");
    else
	smpp_server->config_filename = octstr_create(argv[cf_index]);
    
    debug("smpp", 0, "Initializing configuration file %s", octstr_get_cstr(smpp_server->config_filename));
    
    smpp_server_reconfigure(smpp_server);

    report_versions("vsmppbox");


    smpp_server->server_status = SMPP_SERVER_STATUS_RUNNING;
    
    smpp_listener_start(smpp_server); // This request will block until stopped
    
    smpp_server_destroy(smpp_server);
   

    log_close_all();
    gwlib_shutdown();

    return 0;
}

