This is a derivative of ksmppd which is developed by Kurt and now maintained by Donald Jackson : https://github.com/kneodev/ksmppd


There are no bearerbox connections alive for deliver of messages.

    submit_sm (message submissions) will be queued to the database for later delivery and successful responses returned.
    Any queued MO/DLR's from a previously alive bearerbox will be forwarded to connected ESME's.

At least one bearerbox is alive but no ESME's (or some receivers) are not available

    MO/DLR's received from bearerbox will be attempt to be routed to a target receiver ESME - if not available they will be queued to try later.
    Once a previously unavailable ESME becomes available, pending MO/DLR's will be forwarded.
    It's important to note that there is an 'open ack' limit to ESME's, and if an ESME hits this limit, messages will begin being queued to the database.

System is behaving as normal (database up, bearerbox(s) up, ESME's available)

    If a bearerbox rejects a message (invalid routing, etc) - the ESME will get this result immediately via submit_sm_resp
    If permanent routing failure for MO (eg: no routes) messages will be discarded and bearerbox notified accordingly (ack_failed)
    If an ESME exceeds their max allowed open acks, messages will be queued to the database and requeued as space becomes available. This is to solve excessive memory use.

System restart

    The system first starts, connects to bearerbox and allows connections from ESMEs. Once started it begins reprocessing bearerbox queues if any.
    Once ESME's reconnect - their queued messages (in database) will begin being reprocessed.